#[cfg(feature = "js")]
use crate::wasm_task_manager::WebSysTaskManager as TaskManager;

#[cfg(feature = "sys")]
use crate::tokio_task_manager::TokioTaskManager as TaskManager;

use std::{
    collections::HashMap,
    io::Write,
    sync::{mpsc::Sender, Arc},
};

use waef_core::{
    actors::{definition::ActorDefinition, ActorAlignment, IsActor},
    core::{Message, WaefError},
    ecs::EcsRegistry,
    ResourceStore,
};
use wasmer::{imports, Engine, Function, FunctionEnv, Instance, Memory, Module, Store, Table};

use wasmer_wasix::{
    fs::FallbackFileSystem,
    virtual_fs::{DualWriteFile, NullFile},
    Pipe, PluggableRuntime, WasiEnv,
};
pub enum WasmActorCallback {
    OnMessage(Function),
    OnDispose(Function),
}

struct WasmActorData {
    memory: Memory,
    indirect_function_table: Table,
    indirect_function_cache: HashMap<u32, Function>,
    actor_builder: Option<ActorDefinition<WasmActorContext, (WasmActorCallback, u32)>>,
    dispatcher: Sender<Message>,
    ecs: Arc<dyn EcsRegistry>,
    resources: Arc<dyn ResourceStore>,
    stdin: Pipe,
}

struct WasmActorContext {
    store: Store,
    stdin: Pipe,
}

mod wasm {
    use wasmer::Function;
    use wasmer::{AsStoreMut, MemoryView, Table};

    fn u128_to_high_low(value: u128) -> (u64, u64) {
        ((value >> 64) as u64, (value as u64))
    }

    fn high_low_to_u128(high: u64, low: u64) -> u128 {
        ((high as u128) << 64) | (low as u128)
    }

    fn read_array_of_ptr32(view: &MemoryView, array_ptr: u64, ptr_len: u64) -> Vec<u64> {
        let mut result = Vec::with_capacity(ptr_len as usize);
        for i in 0..ptr_len {
            let item_start = ((i * 4) + array_ptr) as u64;
            let mut ptr_parts = [0; 4];
            if let Err(err) = view.read(item_start, &mut ptr_parts) {
                tracing::error!("Failed to read memory from array of ptrs: {:?}", err);
            } else {
                result.push(u32::from_le_bytes(ptr_parts) as u64);
            }
        }
        result
    }

    fn read_array_of_u128(view: &MemoryView, array_ptr: u64, ptr_len: u64) -> Vec<u128> {
        let mut result = Vec::with_capacity(ptr_len as usize);
        for i in 0..ptr_len {
            let item_start = ((i * 16) + array_ptr) as u64;
            let mut value_parts = [0; 16];
            if let Err(err) = view.read(item_start, &mut value_parts) {
                tracing::error!("Failed to read memory from array of u128: {:?}", err);
            } else {
                result.push(u128::from_le_bytes(value_parts));
            }
        }
        result
    }

    fn read_utf8_string(view: &MemoryView, string_ptr: u64) -> String {
        let mut string_len = 0;

        let data_end = view.data_size();
        for index in string_ptr..data_end {
            if view.read_u8(index).unwrap_or(0) == 0 {
                string_len = index - string_ptr;
                break;
            }
        }

        let mut string_data = vec![0; string_len as usize];
        if let Err(err) = view.read(string_ptr, &mut string_data) {
            tracing::error!(
                "Failed to read memory from string_ptrs: {:?} (calculated len: {})",
                err,
                string_len
            );
            "".into()
        } else {
            match String::from_utf8(string_data) {
                Ok(res) => res,
                Err(err) => {
                    tracing::error!(
                        "Failed to read parse utf8 string from data: {:?} (calculated len: {})",
                        err,
                        string_len
                    );
                    "".into()
                }
            }
        }
    }

    fn get_function_from_ptr(
        store: &mut impl AsStoreMut,
        table: &Table,
        function_ptr: u32,
    ) -> Option<Function> {
        table.get(store, function_ptr)?.funcref()?.clone()
    }

    pub mod actor {
        use crate::from_bytes::{WasmActorCallback, WasmActorData};
        use std::io::Write;
        use waef_core::core::{DispatchFilter, ExecutionResult, Message};
        use wasmer::{AsStoreMut, FunctionEnvMut, Value};

        use super::{get_function_from_ptr, read_array_of_ptr32, read_utf8_string};

        fn impl_register_on_message(
            mut store: impl AsStoreMut,
            data: &mut WasmActorData,
            callback_fid: u32,
            messages_filter: DispatchFilter,
            context_ptr: u32,
        ) {
            match get_function_from_ptr(&mut store, &data.indirect_function_table, callback_fid) {
                Some(callback) => {
                    data.actor_builder.as_mut().unwrap().on_message(
                        messages_filter,
                        (WasmActorCallback::OnMessage(callback), context_ptr),
                        |ctx, (callback, context_ptr), msg| -> ExecutionResult {
                            if let Err(err) = ctx.stdin.write(&msg.name.as_bytes()[..]) {
                                tracing::error!("Failed to write message name to stdin {:?}", err);
                                return ExecutionResult::Kill;
                            }
                            if let Err(err) = ctx.stdin.write(&msg.buffer[..]) {
                                tracing::error!("Failed to write message body to stdin {:?}", err);
                                return ExecutionResult::Kill;
                            }
                            if let WasmActorCallback::OnMessage(callback) = callback {
                                return match callback.call(
                                    &mut ctx.store,
                                    &[
                                    Value::I32(*context_ptr as i32),
                                    Value::I32(msg.name.len() as i32),
                                    Value::I32(msg.buffer.len() as i32),
                                    ]
                                ) {
                                    Ok(execution_result) => match execution_result.get(0).and_then(|x| x.i32()) {
                                        Some(0) => ExecutionResult::NoOp,
                                        Some(1) => ExecutionResult::Processed,
                                        Some(2) => ExecutionResult::Kill,
                                        _ => ExecutionResult::Processed,
                                    },
                                    Err(err) => {
                                        tracing::error!(
                                            "Failed to invoke register_on_message callback: {:?}",
                                            err
                                        );
                                        ExecutionResult::Kill
                                    }
                                };
                            } else {
                                tracing::error!("Failed to invoke register_on_message callback: OnDispose somehow provided?");
                                ExecutionResult::Kill
                            }
                        },
                    );
                }
                None => {
                    tracing::error!(
                        "Failed to retrieve function ptr for register_on_message callback"
                    );
                }
            }
        }

        fn impl_register_on_dispose(
            mut store: impl AsStoreMut,
            data: &mut WasmActorData,
            callback_fid: u32,
            context_ptr: u32,
        ) {
            match get_function_from_ptr(&mut store, &data.indirect_function_table, callback_fid) {
                Some(callback) => {
                    data.actor_builder.as_mut().unwrap().on_dispose(
                        (WasmActorCallback::OnDispose(callback), context_ptr),
                        |ctx, (callback, context_ptr)| {
                            if let WasmActorCallback::OnDispose(callback) = callback {
                                return match callback.call(&mut ctx.store, &[Value::I32(*context_ptr as i32)]) {
                                    Ok(_) => {}
                                    Err(err) => {
                                        tracing::error!(
                                            "Failed to invoke register_on_dispose callback: {:?}",
                                            err
                                        );
                                    }
                                };
                            } else {
                                tracing::error!("Failed to invoke register_on_dispose callback: OnMessage somehow provided?");
                            }
                        },
                    );
                }
                None => {
                    tracing::error!(
                        "Failed to retrieve function ptr for register_on_dispose callback"
                    );
                }
            }
        }

        pub fn register_on_one_of_messages(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            message_names_array_ptr: u32,
            message_names_len: u32,
            callback_fid: u32,
            context_ptr: u32,
        ) {
            let (data, mut store) = env.data_and_store_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let view = data.memory.view(&mut store);

            let messages_filter = DispatchFilter::OneOf(
                read_array_of_ptr32(
                    &view,
                    message_names_array_ptr as u64,
                    message_names_len as u64,
                )
                .into_iter()
                .map(|ptr| read_utf8_string(&view, ptr))
                .collect(),
            );

            impl_register_on_message(store, data, callback_fid, messages_filter, context_ptr);
        }

        pub fn register_on_message(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            message_name_ptr: u32,
            callback_fid: u32,
            context_ptr: u32,
        ) {
            let (data, mut store) = env.data_and_store_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let view = data.memory.view(&mut store);

            let messages_filter =
                DispatchFilter::OneOf([read_utf8_string(&view, message_name_ptr as u64)].into());

            impl_register_on_message(store, data, callback_fid, messages_filter, context_ptr);
        }

        pub fn register_on_dispose(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            callback_fid: u32,
            context_ptr: u32,
        ) {
            let (data, store) = env.data_and_store_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();

            impl_register_on_dispose(store, data, callback_fid, context_ptr);
        }

        pub fn dispatch(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            message_name_ptr: u32,
            message_data_ptr: u32,
            message_data_len: u32,
        ) {
            let (data, store) = env.data_and_store_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let view = data.memory.view(&store);

            let message_name = read_utf8_string(&view, message_name_ptr as u64);
            let mut message_data_buffer = vec![0; message_data_len as usize];
            if let Err(err) = view.read(message_data_ptr as u64, &mut message_data_buffer) {
                tracing::error!("Failed to read message data from memory: {:?}", err);
                return;
            }

            _ = data.dispatcher.send(Message::new_from_boxed_bytes(
                message_name,
                message_data_buffer.into_boxed_slice(),
            ));
        }

        pub fn dispatch_headered(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            message_name_ptr: u32,
            message_header_ptr: u32,
            message_header_len: u32,
            message_body_ptr: u32,
            message_body_len: u32,
        ) {
            let (data, store) = env.data_and_store_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let view = data.memory.view(&store);

            let message_name = read_utf8_string(&view, message_name_ptr as u64);
            let mut message_data_buffer = vec![0; (message_header_len + message_body_len) as usize];
            if let Err(err) = view.read(
                message_header_ptr as u64,
                &mut message_data_buffer[0..message_header_len as usize],
            ) {
                tracing::error!("Failed to read message header from memory: {:?}", err);
                return;
            }
            if let Err(err) = view.read(
                message_body_ptr as u64,
                &mut message_data_buffer[message_body_len as usize..],
            ) {
                tracing::error!("Failed to read message body from memory: {:?}", err);
                return;
            }

            _ = data.dispatcher.send(Message::new_from_boxed_bytes(
                message_name,
                message_data_buffer.into_boxed_slice(),
            ));
        }
    }

    pub mod ecs {
        use std::{collections::HashMap, io::Write};
        use waef_core::{core::WaefError, util::pod::Pod};
        use wasmer::{AsStoreMut, Function, FunctionEnvMut, Memory, Table, Value};

        use crate::from_bytes::{
            wasm::{read_array_of_ptr32, read_utf8_string},
            WasmActorData,
        };

        use super::{
            get_function_from_ptr, high_low_to_u128, read_array_of_u128, u128_to_high_low,
        };

        pub fn create_entity(env: FunctionEnvMut<Option<WasmActorData>>, high: u64, low: u64) {
            let data = env.data().as_ref().unwrap();

            let entity_id = high_low_to_u128(high, low);
            if let Err(err) = data.ecs.create_entity(entity_id) {
                tracing::error!("creating entity with id {} failed: {}", entity_id, err);
            }
        }

        pub fn destroy_entity(env: FunctionEnvMut<Option<WasmActorData>>, high: u64, low: u64) {
            let data = env.data().as_ref().unwrap();

            let entity_id = high_low_to_u128(high, low);
            if let Err(err) = data.ecs.destroy_entity(entity_id) {
                tracing::error!("destroying entity with id {} failed: {}", entity_id, err);
            }
        }

        pub fn define_component(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            component_name_ptr: u32,
            size: u32,
        ) {
            let (data, store) = env.data_and_store_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let view = data.memory.view(&store);
            let component_name = read_utf8_string(&view, component_name_ptr as u64);

            if let Err(err) = data
                .ecs
                .define_component_by_size(component_name, size as usize)
            {
                tracing::error!("Error when defining component: {}", err);
            }
        }

        pub fn list_component_definitions(mut env: FunctionEnvMut<Option<WasmActorData>>) -> u32 {
            let data = env.data_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();

            let mut data_size: usize = 0;
            let comp_definitions = data.ecs.list_component_definitions();
            for (component_name, component_size) in comp_definitions {
                _ = data.stdin.write(&component_name.as_bytes()[..]);
                _ = data.stdin.write(&[b'\0']);
                _ = data.stdin.write((component_size as u32).as_bytes());

                data_size += component_name.len() + 1 + 4;
            }
            data_size as u32
        }

        pub fn get_component_size(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            component_name_ptr: u32,
        ) -> u32 {
            let (data, store) = env.data_and_store_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let view = data.memory.view(&store);
            let component_name = read_utf8_string(&view, component_name_ptr as u64);

            data.ecs.get_component_size(component_name).unwrap_or(0) as u32
        }

        pub fn attach_component(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            eid_high: u64,
            eid_low: u64,
            component_name_ptr: u32,
            component_data_ptr: u32,
            component_data_len: u32,
        ) {
            let entity_id = high_low_to_u128(eid_high, eid_low);

            let (data, store) = env.data_and_store_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let view = data.memory.view(&store);
            let component_name = read_utf8_string(&view, component_name_ptr as u64);

            let mut component_data = vec![0; component_data_len as usize];
            if let Err(err) = view.read(component_data_ptr as u64, &mut component_data) {
                tracing::error!("Failed to read memory when attaching component: {:?}", err);
                return;
            }

            if let Err(err) = data.ecs.attach_component_buffer(
                component_name,
                entity_id,
                component_data.into_boxed_slice(),
            ) {
                tracing::error!("Attaching a component to an entity failed: {:?}", err);
            }
        }

        pub fn detach_component(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            eid_high: u64,
            eid_low: u64,
            component_name_ptr: u32,
        ) {
            let entity_id = high_low_to_u128(eid_high, eid_low);

            let (data, store) = env.data_and_store_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let view = data.memory.view(&store);
            let component_name = read_utf8_string(&view, component_name_ptr as u64);

            if let Err(err) = data.ecs.detach_component_by_name(component_name, entity_id) {
                tracing::error!("Detaching a component to an entity failed: {:?}", err);
            }
        }

        fn apply_components_unsafe(
            mut store: impl AsStoreMut,
            indirect_function_table: &Table,
            indirect_function_cache: &mut HashMap<u32, Function>,
            memory: &mut Memory,
            callback_ptr: u32,
            context: u32,
            entity_id: u128,
            buffer_ptr: u32,
            components: &[(*mut u8, usize)],
            buffer_alignment: u32,
        ) {
            let (high, low) = u128_to_high_low(entity_id);

            let view = memory.view(&store);
            _ = components.iter().fold(
                buffer_ptr as usize,
                |index, (component_ptr, component_size)| {
                    let buffer_alignment = buffer_alignment as usize;
                    if !component_ptr.is_null() {
                        if let Err(err) = view.write(index as u64, unsafe {
                            std::slice::from_raw_parts_mut(*component_ptr, *component_size)
                        }) {
                            tracing::error!("Failed to write component to buffer: {:?}", err);
                        }
                    } else {
                        if let Err(err) = view.write(index as u64, &vec![0u8; *component_size]) {
                            tracing::error!("Failed to write component to buffer: {:?}", err);
                        }
                    }
                    index
                        + component_size
                        + ((buffer_alignment - (component_size % buffer_alignment))
                            % buffer_alignment)
                },
            );

            match indirect_function_cache
                .get(&callback_ptr)
                .cloned()
                .or_else(|| {
                    let function = get_function_from_ptr(&mut store, indirect_function_table, callback_ptr)?;
                    indirect_function_cache.insert(callback_ptr, function.clone());
                    Some(function)
                })
                .map(|funcref| -> Result<(), WaefError> {
                    funcref
                        .call(
                            &mut store,
                            &[
                                Value::I32(context as i32),
                                Value::I64(high as i64),
                                Value::I64(low as i64),
                                Value::I32(buffer_ptr as i32),
                            ],
                        )
                        .map_err(WaefError::new)?;

                    Ok(())
                }) {
                Some(Ok(_)) => {
                    let view = memory.view(&store);
                    _ = components.into_iter().fold(
                        buffer_ptr as usize,
                        |index, (component_ptr, component_size)| {
                            let buffer_alignment = buffer_alignment as usize;

                            if !component_ptr.is_null() {
                                if let Err(err) = view.read(index as u64, unsafe {
                                    std::slice::from_raw_parts_mut(*component_ptr, *component_size)
                                }) {
                                    tracing::error!(
                                        "Failed to read component from buffer: {:?}",
                                        err
                                    );
                                }
                            }
                            index
                                + component_size
                                + ((buffer_alignment - (component_size % buffer_alignment))
                                    % buffer_alignment)
                        },
                    );
                }
                Some(Err(err)) => {
                    tracing::error!("Failed to invoke apply: {:?}", err);
                }
                None => {
                    tracing::error!("Failed to invoke apply");
                }
            }
        }

        pub fn apply(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            buffer_ptr: u32,
            _buffer_size: u32,
            buffer_alignment: u32,
            component_names_ptr: u32,
            component_names_count: u32,
            callback_ptr: u32,
            context: u32,
        ) {
            let (data, mut store) = env.data_and_store_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let view = data.memory.view(&store);

            let component_names: Vec<String> = read_array_of_ptr32(
                &view,
                component_names_ptr as u64,
                component_names_count as u64,
            )
            .into_iter()
            .map(|ptr| read_utf8_string(&view, ptr))
            .collect();

            data.ecs
                .apply_unsafe(&component_names, &mut |entity_id, components| {
                    apply_components_unsafe(
                        &mut store,
                        &data.indirect_function_table,
                        &mut data.indirect_function_cache,
                        &mut data.memory,
                        callback_ptr,
                        context,
                        entity_id,
                        buffer_ptr,
                        components,
                        buffer_alignment,
                    );
                });
        }

        pub fn apply_single(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            eid_high: u64,
            eid_low: u64,
            buffer_ptr: u32,
            _buffer_size: u32,
            buffer_alignment: u32,
            component_names_ptr: u32,
            component_names_count: u32,
            callback_ptr: u32,
            context: u32,
        ) {
            let entity_id = high_low_to_u128(eid_high, eid_low);

            let (data, mut store) = env.data_and_store_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let view = data.memory.view(&store);

            let component_names: Vec<String> = read_array_of_ptr32(
                &view,
                component_names_ptr as u64,
                component_names_count as u64,
            )
            .into_iter()
            .map(|ptr| read_utf8_string(&view, ptr))
            .collect();

            data.ecs.apply_single_unsafe(
                entity_id,
                &component_names,
                &mut |entity_id, components| {
                    apply_components_unsafe(
                        &mut store,
                        &data.indirect_function_table,
                        &mut data.indirect_function_cache,
                        &mut data.memory,
                        callback_ptr,
                        context,
                        entity_id,
                        buffer_ptr,
                        components,
                        buffer_alignment,
                    );
                },
            );
        }

        pub fn apply_set(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            entity_ids_ptr: u32,
            entity_ids_count: u32,
            buffer_ptr: u32,
            _buffer_size: u32,
            buffer_alignment: u32,
            component_names_ptr: u32,
            component_names_count: u32,
            callback_ptr: u32,
            context: u32,
        ) {
            let (data, mut store) = env.data_and_store_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let view = data.memory.view(&store);

            let entity_ids: Vec<u128> =
                read_array_of_u128(&view, entity_ids_ptr as u64, entity_ids_count as u64)
                    .into_iter()
                    .collect();

            let component_names: Vec<String> = read_array_of_ptr32(
                &view,
                component_names_ptr as u64,
                component_names_count as u64,
            )
            .into_iter()
            .map(|ptr| read_utf8_string(&view, ptr))
            .collect();

            data.ecs.apply_set_unsafe(
                &entity_ids,
                &component_names,
                &mut |entity_id, components| {
                    apply_components_unsafe(
                        &mut store,
                        &data.indirect_function_table,
                        &mut data.indirect_function_cache,
                        &mut data.memory,
                        callback_ptr,
                        context,
                        entity_id,
                        buffer_ptr,
                        components,
                        buffer_alignment,
                    );
                },
            );
        }
    }

    pub mod resources {
        use super::high_low_to_u128;
        use crate::from_bytes::WasmActorData;
        use std::io::Write;
        use wasmer::FunctionEnvMut;

        pub fn get_size(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            resource_key_high: u64,
            resource_key_low: u64,
        ) -> u32 {
            let data = env.data_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let resource_key = high_low_to_u128(resource_key_high, resource_key_low);

            data.resources.get_size(resource_key).unwrap_or(0) as u32
        }

        pub fn get(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            resource_key_high: u64,
            resource_key_low: u64,
            target_ptr: u32,
            target_len: u32,
        ) -> u32 {
            let (data, store) = env.data_and_store_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let resource_key = high_low_to_u128(resource_key_high, resource_key_low);
            let view = data.memory.view(&store);

            let mut target = vec![0; target_len as usize];

            if let Some(result) = data.resources.get(resource_key, &mut target[..]) {
                if let Err(err) = view.write(target_ptr as u64, &target[..]) {
                    tracing::error!("Failed to write to target from buffer: {:?}", err);
                    0
                } else {
                    result as u32
                }
            } else {
                0
            }
        }

        pub fn get_slice(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            resource_key_high: u64,
            resource_key_low: u64,
            offset_index: u32,
            target_ptr: u32,
            target_len: u32,
        ) -> u32 {
            let (data, store) = env.data_and_store_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let resource_key = high_low_to_u128(resource_key_high, resource_key_low);
            let view = data.memory.view(&store);

            let mut target = vec![0; target_len as usize];

            if let Some(result) =
                data.resources
                    .get_slice(resource_key, offset_index as usize, &mut target[..])
            {
                if let Err(err) = view.write(target_ptr as u64, &target[..]) {
                    tracing::error!("Failed to write to target from buffer: {:?}", err);
                    0
                } else {
                    result as u32
                }
            } else {
                0
            }
        }

        pub fn put(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            resource_key_high: u64,
            resource_key_low: u64,
            source_ptr: u32,
            source_ptr_len: u32,
        ) -> u32 {
            let (data, store) = env.data_and_store_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let resource_key = high_low_to_u128(resource_key_high, resource_key_low);
            let view = data.memory.view(&store);

            let mut view_data = vec![0; source_ptr_len as usize];
            if let Err(err) = view.read(source_ptr as u64, &mut view_data[..]) {
                tracing::error!("Failed to read put source from buffer: {:?}", err);
                0
            } else {
                data.resources.put(resource_key, &view_data);
                view_data.len() as u32
            }
        }

        pub fn put_slice(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            resource_key_high: u64,
            resource_key_low: u64,
            target_index: u32,
            source_ptr: u32,
            source_ptr_len: u32,
        ) -> u32 {
            let (data, store) = env.data_and_store_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let resource_key = high_low_to_u128(resource_key_high, resource_key_low);
            let view = data.memory.view(&store);

            let mut view_data = vec![0; source_ptr_len as usize];
            if let Err(err) = view.read(source_ptr as u64, &mut view_data[..]) {
                tracing::error!("Failed to read put source from buffer: {:?}", err);
                0
            } else {
                data.resources
                    .put_slice(resource_key, target_index as usize, &view_data);
                view_data.len() as u32
            }
        }

        pub fn get_cloned(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            resource_key_high: u64,
            resource_key_low: u64,
        ) -> u32 {
            let data = env.data_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let resource_key = high_low_to_u128(resource_key_high, resource_key_low);

            if let Some(result) = data.resources.get_cloned(resource_key) {
                if let Err(err) = data.stdin.write(&result) {
                    tracing::error!("Failed to write cloned resource from buffer: {:?}", err);
                    0
                } else {
                    result.len() as u32
                }
            } else {
                0
            }
        }

        pub fn remove(
            mut env: FunctionEnvMut<Option<WasmActorData>>,
            resource_key_high: u64,
            resource_key_low: u64,
        ) -> u32 {
            let data = env.data_mut();
            let data: &mut WasmActorData = (*data).as_mut().unwrap();
            let resource_key = high_low_to_u128(resource_key_high, resource_key_low);

            if let Some(result) = data.resources.remove(resource_key) {
                if let Err(err) = data.stdin.write(&result) {
                    tracing::error!("Failed to write removed resource from buffer: {:?}", err);
                    0
                } else {
                    result.len() as u32
                }
            } else {
                0
            }
        }
    }
}

struct LogWriteInfo {
    name: String,
    data: Vec<u8>,
}

impl LogWriteInfo {
    pub fn new(name: impl Into<String>) -> Self {
        Self {
            name: name.into(),
            data: Vec::with_capacity(1024),
        }
    }
}

impl std::io::Write for LogWriteInfo {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let len = buf.len();
        self.data.append(&mut buf.to_vec());
        if buf.contains(&b'\n') {
            self.flush()?;
        }
        Ok(len)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        let data = std::mem::replace(&mut self.data, Vec::with_capacity(1024));
        if let Ok(output) = String::from_utf8(data) {
            tracing::info!(target: "waef:wasm:actor", "actor_log={}: {}", self.name, output.trim_end_matches('\n'));
            Ok(())
        } else {
            Err(std::io::Error::other("Could not flush to utf-8"))
        }
    }
}

struct LogWriteErr {
    name: String,
    data: Vec<u8>,
}

impl LogWriteErr {
    pub fn new(name: impl Into<String>) -> Self {
        Self {
            name: name.into(),
            data: Vec::with_capacity(1024),
        }
    }
}

impl std::io::Write for LogWriteErr {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let len = buf.len();
        self.data.append(&mut buf.to_vec());
        if buf.contains(&b'\n') {
            self.flush()?;
        }
        Ok(len)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        let data = std::mem::replace(&mut self.data, Vec::with_capacity(1024));
        if let Ok(output) = String::from_utf8(data) {
            tracing::error!(target: "waef:wasm:actor", "actor_error={}: {}", self.name, output.trim_end_matches('\n'));
            Ok(())
        } else {
            Err(std::io::Error::other("Could not flush to utf-8"))
        }
    }
}

pub fn new_wasm_actor_from_bytes(
    dispatcher: Sender<Message>,
    ecs: Arc<dyn EcsRegistry>,
    resources: Arc<dyn ResourceStore>,
    name: impl Into<String>,
    weight: u32,
    alignment: ActorAlignment,
    wasm: Vec<u8>,
) -> Result<Box<dyn IsActor>, WaefError> {
    let name: String = name.into();

    let _guard = tracing::trace_span!(
        "wasmer_actor::new_from_bytes",
        actor = name,
        wasm_size = wasm.len()
    )
    .entered();

    #[cfg(all(
        feature = "sys",
        any(feature = "llvm", feature = "cranelift", feature = "singlepass")
    ))]
    let (mut store, module) = {
        use wasmer::sys::NativeEngineExt;

        let compiled_bytes = {
            let _guard = tracing::trace_span!(
                "wasmer_actor::new_from_bytes::compile_module",
                actor = name,
                wasm_size = wasm.len()
            )
            .entered();

            #[cfg(feature = "llvm")]
            let compiler = wasmer_compiler_llvm::LLVM::default();

            #[cfg(feature = "cranelift")]
            let compiler = wasmer_compiler_cranelift::Cranelift::default();

            #[cfg(feature = "singlepass")]
            let compiler = wasmer_compiler_singlepass::Singlepass::default();

            let store = Store::new(compiler);
            let module = Module::new(&store, &wasm).map_err(WaefError::new)?;
            module.serialize().map_err(WaefError::new)?.to_vec()
        };

        let engine = Engine::headless();
        let store = Store::new(engine);
        let module = {
            let _guard = tracing::trace_span!(
                "wasmer_actor::new_from_bytes::parse_module",
                actor = name,
                module_size = wasm.len()
            )
            .entered();
            unsafe { Module::deserialize_unchecked(&store, &compiled_bytes) }
                .map_err(WaefError::new)?
        };
        (store, module)
    };

    #[cfg(any(
        feature = "js",
        feature = "v8",
        feature = "jsc",
        feature = "wamr",
        feature = "wasmi",
        feature = "precompiled"
    ))]
    let (mut store, module) = {
        let engine = Engine::default();
        let store = Store::new(engine);
        let module = {
            let _guard = tracing::trace_span!(
                "wasmer_actor::new_from_bytes::init_module",
                actor = name,
                wasm_size = wasm.len()
            )
            .entered();
            Module::new(&store, &wasm).map_err(WaefError::new)?
        };
        (store, module)
    };

    let (stdin_send, stdin_recv) = Pipe::channel();

    let mut stdout = LogWriteInfo::new(name.clone());
    let mut stderr = LogWriteErr::new(name.clone());

    let mut wasi_env = WasiEnv::builder(name.clone())
        .stdin(Box::new(stdin_recv))
        .stdout(Box::new(DualWriteFile::new(
            Box::new(NullFile::default()),
            move |data| {
                _ = stdout.write(data);
            },
        )))
        .stderr(Box::new(DualWriteFile::new(
            Box::new(NullFile::default()),
            move |data| {
                _ = stderr.write(data);
            },
        )))
        .fs(Box::new(FallbackFileSystem::default()))
        .runtime(Arc::new(PluggableRuntime::new(Arc::new(
            TaskManager::default(),
        ))))
        .finalize(&mut store)
        .map_err(WaefError::new)?;

    let wasi_imports = wasi_env
        .import_object_for_all_wasi_versions(&mut store, &module)
        .map_err(WaefError::new)?;

    let env = FunctionEnv::new(&mut store, None);
    let mut imports_obj = imports! {
        "waef.actor" => {
            "register_on_one_of_messages" => Function::new_typed_with_env(&mut store,  &env, wasm::actor::register_on_one_of_messages),
            "register_on_message" => Function::new_typed_with_env(&mut store,  &env, wasm::actor::register_on_message),
            "register_on_dispose" => Function::new_typed_with_env(&mut store,  &env, wasm::actor::register_on_dispose),
            "dispatch" => Function::new_typed_with_env(&mut store,  &env, wasm::actor::dispatch),
            "dispatch_headered" => Function::new_typed_with_env(&mut store,  &env, wasm::actor::dispatch_headered),
        },
        "waef.ecs" => {
            "create_entity" => Function::new_typed_with_env(&mut store,  &env, wasm::ecs::create_entity),
            "destroy_entity" => Function::new_typed_with_env(&mut store,  &env, wasm::ecs::destroy_entity),
            "define_component" => Function::new_typed_with_env(&mut store,  &env, wasm::ecs::define_component),
            "list_component_definitions" => Function::new_typed_with_env(&mut store,  &env, wasm::ecs::list_component_definitions),
            "get_component_size" => Function::new_typed_with_env(&mut store,  &env, wasm::ecs::get_component_size),
            "attach_component" => Function::new_typed_with_env(&mut store,  &env, wasm::ecs::attach_component),
            "detach_component" => Function::new_typed_with_env(&mut store,  &env, wasm::ecs::detach_component),
            "apply" => Function::new_typed_with_env(&mut store,  &env, wasm::ecs::apply),
            "apply_single" => Function::new_typed_with_env(&mut store,  &env, wasm::ecs::apply_single),
            "apply_set" => Function::new_typed_with_env(&mut store,  &env, wasm::ecs::apply_set),
        },
        "waef.resources" => {
            "get_size" => Function::new_typed_with_env(&mut store,  &env, wasm::resources::get_size),
            "get" => Function::new_typed_with_env(&mut store,  &env, wasm::resources::get),
            "get_slice" => Function::new_typed_with_env(&mut store,  &env, wasm::resources::get_slice),
            "put" => Function::new_typed_with_env(&mut store,  &env, wasm::resources::put),
            "put_slice" => Function::new_typed_with_env(&mut store,  &env, wasm::resources::put_slice),
            "remove" => Function::new_typed_with_env(&mut store,  &env, wasm::resources::remove),
            "get_cloned" => Function::new_typed_with_env(&mut store,  &env, wasm::resources::get_cloned),
        }
    };
    imports_obj.extend(wasi_imports.into_iter());
    let instance = {
        let _guard = tracing::trace_span!(
            "wasmer_actor::new_from_bytes::create_instance",
            actor = name,
            wasm_size = wasm.len()
        )
        .entered();
        Instance::new(&mut store, &module, &imports_obj).map_err(WaefError::new)?
    };

    let memory = instance
        .exports
        .get_memory("memory")
        .map_err(WaefError::new)?;

    wasi_env
        .initialize_with_memory(&mut store, instance.clone(), Some(memory.clone()), true)
        .map_err(WaefError::new)?;

    env.as_mut(&mut store).replace(WasmActorData {
        dispatcher,
        ecs,
        resources,
        stdin: stdin_send.clone(),

        actor_builder: Some(ActorDefinition::with_weight_and_alignment(
            name.clone(),
            weight,
            alignment,
        )),

        memory: memory.clone(),

        indirect_function_table: instance
            .exports
            .get_table("__indirect_function_table")
            .or_else(|_| instance.exports.get_table("table"))
            .map_err(WaefError::new)?
            .clone(),

        indirect_function_cache: HashMap::new(),
    });

    {
        let _guard = tracing::trace_span!("wasmer_actor::new_from_bytes::initialize", actor = name)
            .entered();
        let init = instance
            .exports
            .get_typed_function::<(), ()>(&mut store, "_start")
            .or_else(|_| {
                instance
                    .exports
                    .get_typed_function::<(), ()>(&mut store, "_initialize")
            })
            .map_err(WaefError::new)?;

        init.call(&mut store).map_err(WaefError::new)?;
    }

    let actor = env
        .as_mut(&mut store)
        .as_mut()
        .unwrap()
        .actor_builder
        .take()
        .unwrap();

    let actor = actor.build(WasmActorContext {
        store,
        stdin: stdin_send,
    });

    Ok(Box::new(actor))
}
