use std::pin::Pin;

use futures::{future::BoxFuture, Future};

use wasmer_wasix::{
    runtime::task_manager::{TaskWasm, TaskWasmRunProperties},
    VirtualTaskManager, WasiFunctionEnv, WasiThreadError,
};

#[derive(Debug, Default)]
pub struct PollsterTaskManager {}
impl VirtualTaskManager for PollsterTaskManager {
    fn sleep_now(
        &self,
        _time: std::time::Duration,
    ) -> Pin<Box<dyn Future<Output = ()> + Send + Sync + 'static>> {
        Box::pin(async {})
    }

    fn task_shared(
        &self,
        task: Box<dyn FnOnce() -> BoxFuture<'static, ()> + Send + 'static>,
    ) -> Result<(), WasiThreadError> {
        pollster::block_on(task());
        Ok(())
    }

    fn task_wasm(&self, task: TaskWasm) -> Result<(), WasiThreadError> {
        let run = task.run;
        let recycle = task.recycle;
        let (ctx, store) = WasiFunctionEnv::new_with_store(
            task.module,
            task.env,
            None,
            task.spawn_type,
            task.update_layout,
        )?;

        // If we have a trigger then we first need to run
        // the poller to completion
        if let Some(trigger) = task.trigger {
            pollster::block_on(async {
                let result = trigger().await;
                // Build the task that will go on the callback
                // Invoke the callback
                run(TaskWasmRunProperties {
                    ctx,
                    store,
                    trigger_result: Some(result),
                    recycle,
                });
            });
        } else {
            // Invoke the callback
            run(TaskWasmRunProperties {
                ctx,
                store,
                trigger_result: None,
                recycle,
            });
        }
        Ok(())
    }

    fn task_dedicated(
        &self,
        task: Box<dyn FnOnce() + Send + 'static>,
    ) -> Result<(), WasiThreadError> {
        task();
        Ok(())
    }

    fn thread_parallelism(&self) -> Result<usize, WasiThreadError> {
        Ok(1)
    }
}
