use std::{
    fs::File,
    io::{Read, Seek, SeekFrom},
    sync::{mpsc::Sender, Arc},
};

use waef_core::{
    actors::{ActorAlignment, IsActor},
    core::{Message, WaefError},
    ecs::EcsRegistry,
    ResourceStore,
};

pub fn new_wasm_actor_from_file(
    dispatcher: Sender<Message>,
    ecs: Arc<dyn EcsRegistry>,
    resources: Arc<dyn ResourceStore>,
    name: impl Into<String>,
    weight: u32,
    alignment: ActorAlignment,
    file: &mut File,
) -> Result<Box<dyn IsActor>, WaefError> {
    let name = name.into();

    let _guard = tracing::trace_span!(
        "wasmer_actor::new_wasm_actor_from_file",
        actor = name,
        weight
    )
    .entered();

    let file_buffer = {
        let _guard = tracing::trace_span!("wasmer_actor::new_wasm_actor_from_file::read_file", actor = name, weight).entered();
    
        file.seek(SeekFrom::End(0)).map_err(WaefError::new)?;
        let mut file_buffer =
            Vec::with_capacity(file.stream_position().map_err(WaefError::new)? as usize);

        file.seek(std::io::SeekFrom::Start(0))
            .map_err(WaefError::new)?;
        file.read_to_end(&mut file_buffer).map_err(WaefError::new)?;
        file_buffer
    };

    crate::from_bytes::new_wasm_actor_from_bytes(
        dispatcher,
        ecs,
        resources,
        name,
        weight,
        alignment,
        file_buffer,
    )
}
