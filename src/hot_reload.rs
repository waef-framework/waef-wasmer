use std::{
    ffi::OsString,
    fs::File,
    path::Path,
    sync::{mpsc::Sender, Arc},
    time::SystemTime,
};

use waef_core::{
    actors::{ActorAlignment, HasActorAlignment, IsActor},
    core::{
        DispatchFilter, ExecutionResult, IsDispatcher, IsDisposable, IsMessage, Message, WaefError,
    },
    ecs::EcsRegistry,
    timers::messages::OnAppTick,
    ResourceStore,
};

struct HotReloadWasmActor {
    dispatcher: Sender<Message>,
    ecs: Arc<dyn EcsRegistry>,
    resources: Arc<dyn ResourceStore>,

    name: String,
    weight: u32,
    alignment: ActorAlignment,
    actor: Box<dyn IsActor>,
    wasm_file_path: OsString,
    wasm_file_last_modified: SystemTime,
    hot_reload_cooldown: f64,
}
impl HotReloadWasmActor {
    fn try_hot_reload(&mut self) {
        match File::open(&self.wasm_file_path) {
            Ok(mut wasm_file) => {
                let file_modified = match wasm_file.metadata().and_then(|c| c.modified()) {
                    Ok(file_modified) => file_modified,
                    Err(err) => {
                        tracing::error!("[Hot Reload] Failed to read modified metadata of wasm file for actor '{}': {:?}", self.name, err);
                        self.wasm_file_last_modified
                    }
                };

                if self.wasm_file_last_modified < file_modified {
                    tracing::trace_span!("wasmer_actor::hot_reload", actor = self.name);

                    self.wasm_file_last_modified = file_modified;
                    tracing::info!(
                        "[Hot Reload] actor '{}' was modified ({:?}), hot reloading...",
                        self.name,
                        self.wasm_file_last_modified
                    );

                    match crate::from_file::new_wasm_actor_from_file(
                        self.dispatcher.clone(),
                        self.ecs.clone(),
                        self.resources.clone(),
                        self.name.clone(),
                        self.weight,
                        self.alignment,
                        &mut wasm_file,
                    ) {
                        Ok(actor) => {
                            self.actor.dispatch(&Message::new("hot_reload:unmount"));
                            self.actor.dispose();

                            self.actor = actor;
                            self.actor.dispatch(&Message::new("hot_reload:mount"));

                            tracing::info!(
                                "[Hot Reload] hot reloading of actor {} complete.",
                                self.name
                            );
                        }
                        Err(err) => {
                            tracing::error!("[Hot Reload] failed to hot reload actor: {:?}", err);
                        }
                    }
                }
            }
            Err(err) => {
                tracing::error!("[Hot Reload] failed to open hot reload file: {:?}", err);
            }
        }
    }
}

impl IsDisposable for HotReloadWasmActor {
    fn dispose(&mut self) {
        self.actor.dispose()
    }
}
impl IsDispatcher for HotReloadWasmActor {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if message.name.starts_with(OnAppTick::category_name()) {
            self.hot_reload_cooldown -= OnAppTick::from_message(message)
                .map(|c| c.delta_time)
                .unwrap_or(0.0166);

            if self.hot_reload_cooldown <= 0.0 {
                self.hot_reload_cooldown = 1.0;
                self.try_hot_reload();
            }
        }
        self.actor.dispatch(message)
    }

    fn filter(&self) -> DispatchFilter {
        DispatchFilter::Any
    }
}
impl HasActorAlignment for HotReloadWasmActor {
    fn alignment(&self) -> ActorAlignment {
        self.alignment
    }
}
impl IsActor for HotReloadWasmActor {
    fn weight(&self) -> u32 {
        self.weight
    }

    fn name(&self) -> String {
        self.name.clone()
    }
}

pub fn new_wasm_actor_from_file_hotreload(
    dispatcher: Sender<Message>,
    ecs: Arc<dyn EcsRegistry>,
    resources: Arc<dyn ResourceStore>,
    name: impl Into<String>,
    weight: u32,
    alignment: ActorAlignment,
    file_path: &Path,
) -> Result<Box<dyn IsActor>, WaefError> {
    let name: String = name.into();

    let mut file = File::open(file_path).map_err(WaefError::new)?;

    let initial_actor = crate::from_file::new_wasm_actor_from_file(
        dispatcher.clone(),
        ecs.clone(),
        resources.clone(),
        name.clone(),
        weight,
        alignment,
        &mut file,
    )?;
    let name: String = name.into();

    let file_last_modified = file
        .metadata()
        .map_err(WaefError::new)?
        .modified()
        .map_err(WaefError::new)?;

    Ok(Box::new(HotReloadWasmActor {
        actor: initial_actor,
        alignment,
        dispatcher,
        ecs,
        resources,
        name,
        weight,
        wasm_file_path: OsString::from(file_path.as_os_str()),
        wasm_file_last_modified: file_last_modified,
        hot_reload_cooldown: 1.0,
    }))
}
