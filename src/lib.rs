use std::sync::{mpsc::Sender, Arc};

use serde::{Deserialize, Serialize};
use waef_core::{
    actors::ActorsPlugin, ActorAlignment, EcsRegistry, IsActor, Message, ResourceStore, WaefError,
};

#[cfg(feature = "js")]
mod wasm_task_manager;

#[cfg(feature = "sys")]
mod tokio_task_manager;

mod from_bytes;

#[cfg(feature = "sys")]
mod from_file;

#[cfg(feature = "sys")]
mod hot_reload;

pub struct WasmActorPlugin {
    factory: Box<dyn FnOnce() -> Result<Box<dyn IsActor>, WaefError>>,
}
impl ActorsPlugin for WasmActorPlugin {
    fn apply<T: waef_core::actors::UseActors>(self, target: T) -> T {
        let actor = (self.factory)();
        match actor {
            Ok(actor) => target.use_actor(actor),
            Err(err) => {
                tracing::error!("Failed to initialize actor: {:?}", err);
                target
            }
        }
    }
}

impl WasmActorPlugin {
    pub fn from_bytes(
        dispatcher: Sender<Message>,
        ecs: Arc<dyn EcsRegistry>,
        resources: Arc<dyn ResourceStore>,
        name: impl Into<String>,
        weight: u32,
        alignment: ActorAlignment,
        wasm: Vec<u8>,
    ) -> Self {
        let name: String = name.into();
        Self {
            factory: Box::new(move || {
                crate::from_bytes::new_wasm_actor_from_bytes(
                    dispatcher, ecs, resources, name, weight, alignment, wasm,
                )
            }),
        }
    }
    #[cfg(feature = "sys")]
    pub fn from_file(
        dispatcher: Sender<Message>,
        ecs: Arc<dyn EcsRegistry>,
        resources: Arc<dyn ResourceStore>,
        name: impl Into<String>,
        weight: u32,
        alignment: ActorAlignment,
        mut wasm: std::fs::File,
    ) -> Self {
        let name: String = name.into();
        Self {
            factory: Box::new(move || {
                crate::from_file::new_wasm_actor_from_file(
                    dispatcher, ecs, resources, name, weight, alignment, &mut wasm,
                )
            }),
        }
    }

    #[cfg(feature = "sys")]
    pub fn from_file_hotreload(
        dispatcher: Sender<Message>,
        ecs: Arc<dyn EcsRegistry>,
        resources: Arc<dyn ResourceStore>,
        name: impl Into<String>,
        weight: u32,
        alignment: ActorAlignment,
        wasm: &std::path::Path,
    ) -> Self {
        let name: String = name.into();
        let path = std::ffi::OsString::from(wasm.as_os_str());
        Self {
            factory: Box::new(move || {
                crate::hot_reload::new_wasm_actor_from_file_hotreload(
                    dispatcher,
                    ecs,
                    resources,
                    name,
                    weight,
                    alignment,
                    std::path::Path::new(&path),
                )
            }),
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub struct WasmPluginCfg {
    pub name: String,
    pub path: String,
    pub weight: u32,
    pub alignment: ActorAlignment,
    pub hot_reload: bool,
}

pub struct UseWasmModules {
    modules: Vec<WasmActorPlugin>,
}
impl UseWasmModules {
    pub async fn open(
        dispatcher: Sender<Message>,
        ecs: Arc<dyn EcsRegistry>,
        resources: Arc<dyn ResourceStore>,
        cfg: &Vec<WasmPluginCfg>,
    ) -> Self {
        #[cfg(feature = "sys")]
        {
            let modules = cfg
                .iter()
                .flat_map(|cfg| {
                    if cfg.hot_reload {
                        Some(WasmActorPlugin::from_file_hotreload(
                            dispatcher.clone(),
                            ecs.clone(),
                            resources.clone(),
                            cfg.name.clone(),
                            cfg.weight,
                            cfg.alignment,
                            std::path::Path::new(&cfg.path),
                        ))
                    } else {
                        match std::fs::File::open(&std::path::Path::new(&cfg.path)) {
                            Err(_) => None,
                            Ok(file) => Some(WasmActorPlugin::from_file(
                                dispatcher.clone(),
                                ecs.clone(),
                                resources.clone(),
                                cfg.name.clone(),
                                cfg.weight,
                                cfg.alignment,
                                file,
                            )),
                        }
                    }
                })
                .collect();
            Self { modules }
        }

        #[cfg(feature = "js")]
        {
            let mut modules = Vec::with_capacity(cfg.len());
            for module in cfg {
                match Self::fetch_file(&module.path).await {
                    Err(_err) => (),
                    Ok(file_bytes) => {
                        let plugin = WasmActorPlugin::from_bytes(
                            dispatcher.clone(),
                            ecs.clone(),
                            resources.clone(),
                            module.name.clone(),
                            module.weight,
                            module.alignment,
                            file_bytes,
                        );
                        modules.push(plugin);
                    }
                }
            }

            Self { modules }
        }
    }

    #[cfg(feature = "js")]
    async fn fetch_file(path: &str) -> Result<Vec<u8>, web_sys::wasm_bindgen::JsValue> {
        use wasm_bindgen_futures::JsFuture;
        use web_sys::js_sys::Uint8Array;
        use web_sys::wasm_bindgen::JsCast;
        use web_sys::{Request, RequestInit, RequestMode, Response};

        let opts = RequestInit::new();
        opts.set_method("GET");
        opts.set_mode(RequestMode::SameOrigin);
        let request = Request::new_with_str_and_init(path, &opts)?;

        let resp = JsFuture::from(web_sys::window().unwrap().fetch_with_request(&request)).await?;
        let resp: Response = resp.dyn_into()?;
        let blob = JsFuture::from(resp.array_buffer().unwrap()).await?;
        let array = Uint8Array::new(&blob);
        let file_bytes = array.to_vec();
        Ok(file_bytes)
    }
}
impl ActorsPlugin for UseWasmModules {
    fn apply<T: waef_core::actors::UseActors>(self, target: T) -> T {
        self.modules
            .into_iter()
            .fold(target, |target, plugin| plugin.apply(target))
    }
}

#[cfg(test)]
mod tests {
    use std::sync::{mpsc::channel, Arc};

    use waef_core::{EcsRegistry, ResourceStore};

    use crate::UseWasmModules;

    struct DummyEcsRegistry {}
    
    #[allow(unused)]
    impl EcsRegistry for DummyEcsRegistry {
        fn list_component_definitions(&self) -> std::collections::HashMap<String, usize> {
            todo!()
        }

        fn define_component_by_size(
            &self,
            name: String,
            size: usize,
        ) -> Result<(), waef_core::WaefError> {
            todo!()
        }

        fn get_component_size(&self, name: String) -> Option<usize> {
            todo!()
        }

        fn create_entity(&self, entity_id: u128) -> Result<(), waef_core::WaefError> {
            todo!()
        }

        fn destroy_entity(&self, entity_id: u128) -> Result<(), waef_core::WaefError> {
            todo!()
        }

        fn attach_component_buffer(
            &self,
            component_name: String,
            entity_id: u128,
            component_buffer: Box<[u8]>,
        ) -> Result<(), waef_core::WaefError> {
            todo!()
        }

        fn detach_component_by_name(
            &self,
            component_name: String,
            entity_id: u128,
        ) -> Result<(), waef_core::WaefError> {
            todo!()
        }

        fn apply(
            &self,
            component_query: &[String],
            callback: &mut dyn FnMut(u128, &mut [&mut [u8]]),
        ) {
            todo!()
        }

        fn apply_set(
            &self,
            entity_ids: &[u128],
            component_query: &[String],
            callback: &mut dyn FnMut(u128, &mut [&mut [u8]]),
        ) {
            todo!()
        }

        fn apply_single(
            &self,
            entity_id: u128,
            component_query: &[String],
            callback: &mut dyn FnMut(u128, &mut [&mut [u8]]),
        ) {
            todo!()
        }

        fn apply_set_unsafe(
            &self,
            entity_ids: &[u128],
            component_query: &[String],
            callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
        ) {
            todo!()
        }

        fn apply_unsafe(
            &self,
            component_query: &[String],
            callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
        ) {
            todo!()
        }

        fn apply_single_unsafe(
            &self,
            entity_id: u128,
            component_query: &[String],
            callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
        ) {
            todo!()
        }
    }

    struct DummyResources {}
    
    #[allow(unused)]
    impl ResourceStore for DummyResources {
        fn get_size(&self, key: u128) -> Option<usize> {
            todo!()
        }

        fn get_cloned(&self, key: u128) -> Option<Vec<u8>> {
            todo!()
        }

        fn remove(&self, key: u128) -> Option<Box<[u8]>> {
            todo!()
        }

        fn get(&self, key: u128, buffer: &mut [u8]) -> Option<usize> {
            todo!()
        }

        fn put(&self, key: u128, data: &[u8]) {
            todo!()
        }

        fn put_slice(&self, key: u128, start_index: usize, data: &[u8]) {
            todo!()
        }

        fn get_slice(&self, key: u128, start_index: usize, buffer: &mut [u8]) -> Option<usize> {
            todo!()
        }
    }

    #[test]
    pub fn test_compile_no_plugins() {
        let plugins = vec![];
        let (sender, _receiver) = channel();

        let _result = UseWasmModules::open(
            sender,
            Arc::new(DummyEcsRegistry {}),
            Arc::new(DummyResources {}),
            &plugins,
        );
    }
}
